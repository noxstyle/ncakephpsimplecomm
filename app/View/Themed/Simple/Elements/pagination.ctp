
<div class="paging">
	<?php echo $this->Paginator->first('<< '.__('first'), array(), null, array('class' => 'first disabled')); ?>

	<?php echo $this->Paginator->prev('< '.__('previous'), array(), null, array('class' => 'prev disabled')); ?>

	<?php echo $this->Paginator->numbers(array('separator' => ' ')); ?>

	<?php echo $this->Paginator->next(__('next').' >', array(), null, array('class' => 'next disabled')); ?>

	<?php echo $this->Paginator->last(__('last'). ' >>', array(), null, array('class' => 'last disabled')); ?>

</div>

