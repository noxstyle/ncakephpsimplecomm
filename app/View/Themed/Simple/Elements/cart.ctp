<h4><?php echo __('Cart') ?></h4>
<?php if (is_null($cart) OR count($cart['OrderItem']) == 0): ?>
    <h5><?php echo __('Your cart is empty')?></h5>
<?php else: ?>
    <h5>
        <?php echo __('Cart total')?>:
        <span id="cart-total-sum">
            <?php echo $cart['Order']['total'] ?><?php echo Configure::read('Settings.CURRENCY'); ?>
        </span>
    </h5>
    <table class="table table-responsive table-condensed table-striped table-hover">
        <thead>
            <tr>
                <th><?php echo __('Name') ?></th>
                <th><?php echo __('Qty')?></th>
                <th><?php echo __('Price')?></th>
                <th><i class="glyphicon glyphicon-trash"></i></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($cart['OrderItem'] as $key=>$item): ?>
            <tr>
                <td><?php echo $item['name'] ?></td>
                <td><?php echo $item['quantity'] ?></td>
                <td><?php echo $item['price'] ?><?php echo Configure::read('Settings.CURRENCY'); ?></td>
                <td>
                    <?php echo $this->Html->link('<i class="glyphicon glyphicon-trash"></i>',
                        array('controller' => 'shop', 'action' => 'remove', $key), array('class' => 'btn remove-cart-item', 'escapeTitle'=>false, 'data-id'=>$key));
                    ?>
                </td>
            </tr>
            <?php endforeach; ?>
            <tr></tr>
        </tbody>
    </table>

    <?php echo $this->Html->link(__('View cart').'&raquo;', array('controller' => 'shop', 'action' => 'cart'), array('class' => 'btn', 'escapeTitle'=>false)); ?>
<?php endif ?>

<div id="cart-status"></div>