<div class="product-holder">
    <ul class="products clearfix">
        <?php foreach($products as $product): ?>
        <li class="product col-sm-6 col-md-3">
            <div class="image-holder">
                <?php echo $this->Html->image('/files/product/image/' . $product['Product']['id'] . '/thumb_'.$product['Product']['image'], array('url' => array('controller' => 'products', 'action' => 'view', 'slug' => $product['Product']['slug']), 'alt' => $product['Product']['name'], 'width' => 150, 'height' => 150, 'class' => 'image')); ?>
            </div>
            <div class="info">
                <h2>
                    <?php echo $this->Html->link($product['Product']['name'], array('controller' => 'products', 'action' => 'view', 'slug' => $product['Product']['slug'])); ?>
                </h2>
                <p class="price">
                    <?php echo $product['Product']['price']; ?>
                    <?php echo Configure::read('Settings.CURRENCY'); ?>
                </p>
                <div class="add-to-cart-holder">
                    <?php echo $this->Html->link('<span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;'.__('Add to cart'),
                            array('controller' => 'shop', 'action' => 'add', $product['Product']['id']),
                            array('class' => 'btn add-cart-item', 'escapeTitle' => false, 'data-id' => $product['Product']['id'])); ?>
                </div>
            </div>
        </li>
        <?php endforeach; ?>
    </ul>
</div>