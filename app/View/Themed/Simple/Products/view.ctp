<?php
$this->Html->addCrumb(__('Categories'), '/categories/');
$this->Html->addCrumb($product['Category']['name'], array('controller' => 'categories', 'action' => 'view', 'slug' => $product['Category']['slug']));
$this->Html->addCrumb($product['Product']['name']);
?>

<h1><?php echo $product['Product']['name']; ?></h1>

<div class="row product-info">

	<div class="col col-sm-7">
		<?php

		$imgSrc = 'files/product/image/' . $product['Product']['id'] . '/'.$product['Product']['image'];
		$this->Fancybox->setProperties( array( 
			'class' => 'fancybox3',
            'className' => 'fancybox.image',
            'title'=>$product['Product']['name'],
            'rel' => 'gallery1'
            )
        );

		$this->Fancybox->setPreviewContent($this->Html->image(DS.$imgSrc, array('alt' => $product['Product']['name'], 'class' => 'img-thumbnail img-responsive')));
		$this->Fancybox->setMainContent($this->webroot.$imgSrc);

		echo $this->Fancybox->output();
		?>
	</div>

	<div class="col col-sm-5">

		<p>
			<span class="label label-default"><?php echo __('Category') ?></span> <?php echo $this->Html->link($product['Category']['name'], array('controller' => 'categories', 'action' => 'view', 'slug' => $product['Category']['slug'])); ?>			
		</p>

        <p>
            <span class="label label-default"><?php echo __('Sku') ?></span>
            <?php echo $product['Product']['sku'] ?>
        </p>

		<p>
			<span id="productprice">
				<?php echo $product['Product']['price']; ?><?php echo Configure::read('Settings.CURRENCY'); ?>
			</span>			
		</p>

        <div class="add-to-cart-holder">
            <?php echo $this->Html->link('<span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;'.__('Add to cart'),
                    array('controller' => 'shop', 'action' => 'add', 'slug' => $product['Product']['id']),
                    array('class' => 'btn add-cart-item', 'escapeTitle' => false, 'data-id' => $product['Product']['id'])); ?>
        </div>

		<div class="product-description">
			<?php echo $product['Product']['description']; ?>
		</div>
	</div>

</div>
