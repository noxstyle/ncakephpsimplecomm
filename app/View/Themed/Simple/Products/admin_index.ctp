<?php echo $this->Html->css(array('bootstrap-editable.css', '/select2/select2.css'), 'stylesheet', array('inline' => false)); ?>
<?php echo $this->Html->script(array('bootstrap-editable.js', '/select2/select2.js'), array('inline' => false)); ?>

<script>

$(document).ready(function() {

	$('.category').editable({
		type: 'select',
		name: 'category_id',
		url: '<?php echo $this->webroot; ?>admin/products/editable',
		title: 'Category',
		source: <?php echo json_encode($categorieseditable); ?>,
		placement: 'right',
	});

	$('.name').editable({
		type: 'text',
		name: 'name',
		url: '<?php echo $this->webroot; ?>admin/products/editable',
		title: 'Name',
		placement: 'right',
	});

	$('.price').editable({
		type: 'text',
		name: 'price',
		url: '<?php echo $this->webroot; ?>admin/products/editable',
		title: 'Price',
		placement: 'left',
	});
});
</script>
<h2><?php echo __('Products') ?></h2>

<div class="row">

	<?php echo $this->Form->create('Product', array()); ?>
	<?php echo $this->Form->hidden('search', array('value' => 1)); ?>

	<div class="col-lg-2">
		<?php echo $this->Form->input('active',
			array(
				'label' => false,
				'class' => 'form-control',
				'empty' => __('All Statuses'),
				'options' => array(
					1 => __('Active'),
					0 => __('Not Active')
				),
				'selected' => $all['active']
			)
		); ?>
	</div>

	<div class="col-lg-2">
		<?php echo $this->Form->input('filter', array(
			'label' => false,
			'class' => 'form-control',
			'options' => array(
				'name' => __('Name'),
				'sku' => __('Sku'),
				'description' => __('Description'),
				'price' => __('Price'),
				'created' => __('Created'),
			),
			'selected' => $all['filter']
		)); ?>

	</div>

	<div class="col-lg-2">
		<?php echo $this->Form->input('name', array('label' => false, 'id' => false, 'class' => 'form-control', 'value' => $all['name'])); ?>

	</div>

	<div class="col-lg-4">
		<?php echo $this->Form->button(__('Search'), array('class' => 'btn btn-default')); ?>
		&nbsp; &nbsp;
		<?php echo $this->Html->link(__('Reset Search'), array('controller' => 'products', 'action' => 'reset', 'admin' => true), array('class' => 'btn btn-danger')); ?>

	</div>

	<?php echo $this->Form->end(); ?>

</div>

<br />

<?php echo $this->element('pagination-counter'); ?>

<?php echo $this->element('pagination'); ?>

<br />

<table class="table-striped table-condensed table-bordered table-condensed table-hover table-responsive">
	<tr>
		<th><?php echo $this->Paginator->sort('image'); ?></th>
		<th><?php echo $this->Paginator->sort('category_id'); ?></th>
		<th><?php echo $this->Paginator->sort('name'); ?></th>
		<th><?php echo $this->Paginator->sort('sku'); ?></th>
		<th><?php echo $this->Paginator->sort('slug'); ?></th>
		<th><?php echo $this->Paginator->sort('price'); ?></th>
		<th><?php echo $this->Paginator->sort('tags'); ?></th>
		<th><?php echo $this->Paginator->sort('views'); ?></th>
		<th><?php echo $this->Paginator->sort('active'); ?></th>
		<th class="hidden-sm hidden-xs"><?php echo $this->Paginator->sort('created'); ?></th>
		<th class="hidden-sm hidden-xs"><?php echo $this->Paginator->sort('modified'); ?></th>
		<th class="actions">Actions</th>
	</tr>
	<?php foreach ($products as $product): ?>
	<tr>
		<td><?php echo $this->Html->image('/files/product/image/' . $product['Product']['id'] . '/thumb_'.$product['Product']['image'], array('url' => array('controller' => 'products', 'action' => 'view', 'slug' => $product['Product']['slug']), 'alt' => $product['Product']['name'], 'width' => 100, 'height' => 100, 'class' => 'image')); ?></td>
		<td><span class="category" data-value="<?php echo $product['Category']['id']; ?>" data-pk="<?php echo $product['Product']['id']; ?>"><?php echo $product['Category']['name']; ?></span></td>
		<td><span class="name" data-value="<?php echo $product['Product']['name']; ?>" data-pk="<?php echo $product['Product']['id']; ?>"><?php echo $product['Product']['name']; ?></span></td>
		<td><?php echo $product['Product']['sku'] ?></td>
		<td><?php echo h($product['Product']['slug']); ?></td>
		<td><span class="price" data-value="<?php echo $product['Product']['price']; ?>" data-pk="<?php echo $product['Product']['id']; ?>"><?php echo $product['Product']['price']; ?></span></td>
		<td><span class="tags" data-value="<?php echo $product['Product']['tags']; ?>" data-pk="<?php echo $product['Product']['id']; ?>"><?php echo $product['Product']['tags']; ?></span></td>
		<td><?php echo h($product['Product']['views']); ?></td>
		<td><?php echo $this->Html->link($this->Html->image('icon_' . $product['Product']['active'] . '.png'), array('controller' => 'products', 'action' => 'switch', 'active', $product['Product']['id']), array('class' => 'status', 'escape' => false)); ?></td>
		<td class="hidden-sm hidden-xs"><?php echo $this->App->dateTime($product['Product']['created']) ?></td>
		<td class="hidden-sm hidden-xs"><?php echo $this->App->dateTime($product['Product']['modified']) ?></td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $product['Product']['id']), array('class' => 'btn btn-default btn-xs')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $product['Product']['id']), array('class' => 'btn btn-primary btn-xs')); ?>
		</td>
	</tr>
	<?php endforeach; ?>
</table>

<?php echo $this->element('pagination-counter'); ?>

<?php echo $this->element('pagination'); ?>