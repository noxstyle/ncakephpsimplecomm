<h2><?php echo __('Add Product') ?></h2>

<div class="row">
    <div class="col-md-8">
    <?php $this->Wysiwyg->changeEditor('Ck'); ?>
    <?php echo $this->Form->create('Product', array('type'=>'file')); ?>

        <?php echo $this->Form->input('category_id', array('class' => 'form-control')); ?>

        <?php echo $this->Form->input('name', array('class' => 'form-control')); ?>

        <?php echo $this->Form->input('sku', array('class'=>'form-control')) ?>
        
        <?php echo $this->Form->input('slug', array('class' => 'form-control')); ?>
        
        <?php echo $this->Wysiwyg->textarea('Product.description', array('class'=>'form-control')); ?>
        
        <?php echo $this->Form->input('image', array('class' => 'form-control', 'type'=>'file')); ?>
        
        <?php echo $this->Form->input('price', array('class' => 'form-control')); ?>

        <?php echo $this->Form->input('active', array('type' => 'checkbox')); ?>

        <?php echo $this->Form->button(__('Create'), array('class' => 'btn btn-primary')); ?>
    <?php echo $this->Form->end(); ?>
    </div>
</div>