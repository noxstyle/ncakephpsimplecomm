<?php if($ajax != 1): ?>
    <?php $this->Html->addCrumb(__('Search')); ?>

    <h1>
        <?php echo __('Search') ?>
        <?php if (isset($products) && count($products)>0): ?>
            <span class="small">/ <?php echo __('found %s results', count($products)) ?></span>
        <?php endif; ?>
    </h1>

    <div class="row search-row">
        <?php echo $this->Form->create('Product', array('type' => 'GET')); ?>

        <div class="col col-md-4">
        	<?php echo $this->Form->input('search', array('label' => false, 'div' => false, 'class' => 'form-control', 'autocomplete' => 'off', 'value' => $search)); ?>
        </div>

        <div class="col col-md-3">
            <?php echo $this->Form->input('sort', array(
                'options' => array(
                    'name' => __('Name'),
                    'price' => __('Price'),
                    'views' => __('Views'),
                    'created' => __('Added'),
                ),
                'empty' => __('Sort by'), 
                'class' => 'form-control',
                'div' => false,
                'label' => false,
                'value' => isset($sort) ? $sort : '',
            )); ?>
        </div>

        <div class="col col-md-3">
                <?php echo $this->Form->input('sortDirection', array(
            'options' => array(
                'asc' => __('Ascending'),
                'desc' => __('Descending'),
            ),
            'class' => 'form-control inline',
            'div' => true,
            'label' => false,
            'value' => isset($sortDirection) ? $sortDirection : 'asc',
        )); ?>
        </div>

        <div class="col col-md-2">
        	<?php echo $this->Form->button(__('Search'), array('div' => false, 'class' => 'btn btn-sm btn-primary')); ?>
        </div>

        <?php echo $this->Form->end(); ?>
    </div><!-- /.search-row -->
<?php endif; ?>

<?php if(!empty($search)) : ?>
    <?php $this->Html->addCrumb($search); ?>

    <?php if(!empty($products)) : ?>
        <?php echo $this->element('products'); ?>
    <?php else: ?>
        <h3><?php echo __('No Results') ?></h3>
    <?php endif; ?>

<?php endif; ?>

