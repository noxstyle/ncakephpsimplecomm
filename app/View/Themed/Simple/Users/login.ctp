<div class="login-container">
    <h1><?php echo __('Login') ?></h1>

    <div class="col-sm-4">
    <?php echo $this->Form->create('User', array('action' => 'login')); ?>
        <?php echo $this->Form->input('username', array('class' => 'form-control', 'autofocus' => 'autofocus', 'label' => __('Username'))); ?>
        <?php echo $this->Form->input('password', array('class' => 'form-control', 'label' => __('Password') )); ?>
        <?php echo $this->Form->button(__('Login'), array('class' => 'btn')); ?>
    <?php echo $this->Form->end(); ?>

    </div>
</div><!-- /.login-container -->
