<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

?>
<!doctype html>
<!--[if lt IE 9]><html class="ie"><![endif]-->
<!--[if gt IE 9]><html><![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title><?php echo $title_for_layout; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,800italic,400,700,800' rel='stylesheet' type='text/css'>
	<?php echo $this->Html->css(array('bootstrap.min.css', 'main.css')); ?>
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<!--[if lt IE 9]><?php echo $this->Html->script(array('html5shiv.js', 'respond.min.js')); ?><![endif]-->
	<?php echo $this->Html->script(array('bootstrap.min.js', 'js.js', 'sidecart.js')); ?>
	<?php echo $this->App->js(); ?>
	<?php echo $this->fetch('meta'); ?>
	<?php echo $this->fetch('css'); ?>
	<?php echo $this->fetch('script'); ?>
</head>
<body>

	<header id="header" class="row">
		<div id="top-bar">
			<div class="container">
				<div class="dropdown">
					<a href="#" id="dropdownMenu1" data-toggle="dropdown">
						<i class="glyphicon glyphicon-user"></i>
						<?php echo !$loggedin ? __('Not logged in') : '' ?>
						<span class="caret caret-white"></span>
					</a>
					<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
						<?php if (!$loggedin): ?>
							<li role="presentation">
								<?php echo $this->Html->link(__('Login'),
									array('controller' => 'users', 'action' => 'login'),
									array('role' => 'menuitem', 'tablindex' => '-1')
								); ?>
							</li>
						<?php endif; ?>
					</ul>
				</div>
			</div><!-- /.container -->
		</div><!-- /#top-bar -->
		
		<h1>Shop<span>name</span></h1>
		<h4 class="tagline">Simple eCommerce</h4>
	</header>

	<nav class="navbar navbar-default" role="navigation" id="nav">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
				<span class="sr-only"><?php echo __('Toggle navigation')?></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<ul class="nav navbar-nav">
					<li <?php echo ($this->App->activeNav == 'home') ? 'class="active"' : '' ?>>
						<?php echo $this->Html->link(
							'<i class="glyphicon glyphicon-home"></i>'.__('Home'),
							array('controller' => 'products', 'action' => 'view'),
							array('escapeTitle'=>false)); ?>
					</li>
		          	<?php foreach($categories as $cat): ?>
						<li <?php echo ($this->App->activeNav == $cat['Category']['slug']) ? 'class="active"' : '' ?>>
							<?php echo $this->Html->link($cat['Category']['name'], array('controller' => 'categories', 'action' => 'view', 'slug' => $cat['Category']['slug'])); ?>
						</li>
		          	<?php endforeach; ?>
				</ul>
				<ul class="navbar-form form-inline navbar-right">
					<?php echo $this->Form->create('Product', array('type' => 'GET', 'url' => array('controller' => 'products', 'action' => 'search'))); ?>

					<?php echo $this->Form->input('search', array('label' => false, 'div' => false, 'class' => 'input-md', 'autocomplete' => 'off')); ?>
					<?php echo $this->Form->button(__('Search'), array('div' => false, 'class' => 'btn btn-sm')); ?>
					<?php echo $this->Form->end(); ?>
				</ul>
			</div>
		</div>
	</nav>

	<div class="container">

		<div class="row">

			<div class="col-xs-18 col-sm-12 col-md-9">

				<ul class="breadcrumb">
					<?php echo $this->Html->link(__('Home'), array('controller' => 'products', 'action' => 'index')); ?> / <?php echo $this->Html->getCrumbs(' / '); ?>
				</ul>

				<?php echo $this->Session->flash(); ?>

				<?php echo $this->fetch('content'); ?>
				<div id="msg"></div>
			</div><!-- /span -->

	        <div class="col-xs-18 col-sm-12 col-md-3 sidebar-offcanvas" id="sidebar" role="navigation">
	        	<div id="sidebar-inner">
	        		
		        	<div class="block">
		        		<div id="cart-holder">
							<?php echo $this->element('cart') ?>
		        		</div>
		        	</div><!-- /.block -->

	        	</div><!-- /#sidebar-inner -->

	        </div><!--/span-->
		</div><!-- /.row -->

		<div class="row">


		</div>

	</div><!-- /.container -->
	
    <div id="footer">
      <div class="container">
      	<p class="copy"><?php echo __('Copyright &copy;') ?>&nbsp;ShopName</p>
      </div>
    </div>
</body>
</html>
