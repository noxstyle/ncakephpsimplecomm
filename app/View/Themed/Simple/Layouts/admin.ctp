<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?php echo $title_for_layout; ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php echo $this->Html->css(array('bootstrap.min.css', '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.css', 'main.css')); ?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>

<?php echo $this->Html->script(array('bootstrap.min.js', 'admin.js')); ?>

<?php echo $this->App->js(); ?>

<?php echo $this->fetch('css'); ?>
<?php echo $this->fetch('script'); ?>
</head>
<body class="admin">

	<div class="navbar navbar-static-top" role="navigation">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">SHOP ADMIN</a>
		</div>
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				<li><?php echo $this->Html->link(__('Logout'), array('controller' => 'users', 'action' => 'logout', 'admin' => false)); ?></li>
			</ul>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col col-md-2 well">
				<ul class="nav bs-sidenav">
					<li><?php echo $this->Html->link(__('Products'), array('controller' => 'products', 'action' => 'index', 'admin' => true)); ?></li>
					<ul>
						<li><?php echo $this->Html->link(__('Add Product'), array('controller' => 'products', 'action' => 'add', 'admin' => true)) ?></li>
					</ul>
					<li><?php echo $this->Html->link(__('Categories'), array('controller' => 'categories', 'action' => 'index', 'admin' => true)); ?></li>
					<ul>
						<li><?php echo $this->Html->link(__('Add Category'), array('controller' => 'categories', 'action' => 'add', 'admin' => true)); ?></li>
					</ul>
				</ul>
			</div><!-- /.col -->
			<div class="col col-md-10" role="main">
				<?php echo $this->Session->flash(); ?>
				<?php echo $this->fetch('content'); ?>
			</div><!-- /.col -->
		</div><!-- /.row -->
	</div><!-- /.container -->

    <div id="footer">
      <div class="container">
      	<p class="copy"><?php echo __('Copyright &copy;') ?>&nbsp;ShopName</p>
      </div>
    </div>

</body>
</html>

