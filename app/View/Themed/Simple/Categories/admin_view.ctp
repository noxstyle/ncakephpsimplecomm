
<h2>Category</h2>

<table class="table-striped table-bordered table-condensed table-hover">
	<tr>
		<td><?php echo __('Id') ?></td>
		<td><?php echo h($category['Category']['id']); ?></td>
	</tr>
	<tr>
		<td><?php echo __('Parent Category') ?></td>
		<td><?php echo $this->Html->link($category['ParentCategory']['name'], array('controller' => 'categories', 'action' => 'view', $category['ParentCategory']['id'])); ?></td>
	</tr>
	<tr>
		<td><?php echo __('Lft') ?></td>
		<td><?php echo h($category['Category']['lft']); ?></td>
	</tr>
	<tr>
		<td><?php echo __('Rght') ?></td>
		<td><?php echo h($category['Category']['rght']); ?></td>
	</tr>
	<tr>
		<td><?php echo __('Name') ?></td>
		<td><?php echo h($category['Category']['name']); ?></td>
	</tr>
	<tr>
		<td><?php echo __('Slug') ?></td>
		<td><?php echo h($category['Category']['slug']); ?></td>
	</tr>
	<tr>
		<td><?php echo __('Created') ?></td>
		<td><?php echo $this->App->dateTime($category['Category']['created']); ?></td>
	</tr>
	<tr>
		<td><?php echo __('Modified')?></td>
		<td><?php echo $this->App->dateTime($category['Category']['modified']); ?></td>
	</tr>
</table>

