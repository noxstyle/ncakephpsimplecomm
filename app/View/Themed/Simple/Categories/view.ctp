<?php
$this->App->activeNav = $category['Category']['slug'];
$this->Html->addCrumb(__('Categories'), '/categories/');
foreach ($parents as $parent) {
	$this->Html->addCrumb($parent['Category']['name'], '/category/' . $parent['Category']['slug']);
}
?>

<h1><small><?php echo __('Category') ?></small>&nbsp;<?php echo $category['Category']['name']; ?></h1>

<?php if (!empty($products)): ?>

<?php echo $this->element('products'); ?>

<?php endif; ?>