<h2><?php echo __('Categories') ?></h2>

<?php $this->Html->addCrumb(__('Categories')); ?>

<?php echo $this->Tree->generate($categories, array('element' => 'categories/tree_item', 'class' => 'categorytree', 'id' => 'categorytree')); ?>