<h2><?php echo __('Add Category') ?></h2>

<div class="row">
    <div class="col col-lg-8">

    <?php echo $this->Form->create('Category'); ?>

        <?php echo $this->Form->input('parent_id', array('class' => 'form-control', 'empty' => true, 'label' => __('Parent Category'))); ?>

        <?php echo $this->Form->input('name', array('class' => 'form-control')); ?>

        <?php echo $this->Form->input('slug', array('class' => 'form-control')); ?>

        <?php echo $this->Form->button(__('Create'), array('class' => 'btn')); ?>

    <?php echo $this->Form->end(); ?>

    </div>

</div>