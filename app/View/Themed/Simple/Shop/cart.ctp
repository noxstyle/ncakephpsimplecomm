<?php echo $this->set('title_for_layout', 'Shopping Cart'); ?>

<?php $this->Html->addCrumb('Shopping Cart'); ?>

<?php echo $this->Html->script(array('cart.js'), array('inline' => false)); ?>

<h1><?php echo __('Shopping Cart') ?></h1>

<?php if(empty($shop['OrderItem'])) : ?>

	<p><?php echo __('Shopping Cart is empty') ?></p>
	<p><?php echo $this->Html->link(__('Back to mainpage'), array('controller' => '/', 'action' => 'index')) ?></p>

<?php else: ?>

<?php echo $this->Form->create(NULL, array('url' => array('controller' => 'shop', 'action' => 'cartupdate'))); ?>

<hr>

<div class="shopping-cart-items">
	<table class="table table-striped table-hover table-condensed table-rounded">
		<thead>
			<tr>
				<th>#</th>
				<th><?php echo __('Item') ?></th>
				<th><?php echo __('Price') ?></th>
				<th><?php echo __('Quantity') ?></th>
				<th><?php echo __('Subtotal') ?></th>
				<th><?php echo __('Remove') ?></th>
			</tr>
		</thead>
		<tbody>
			<?php $tabindex = 1; ?>
			<?php foreach ($shop['OrderItem'] as $key => $item): ?>
			<tr id="row-<?php echo $key; ?>">
				<td class="col col-sm-1">
		            <?php echo $this->Html->image(
		            	'/files/product/image/' . $item['Product']['id'] . '/thumb_'.$item['Product']['image'],
		            	array(
		            		'url' => array(
		            			'controller' => 'products',
		            			'action' => 'view',
		            			'slug' => $item['Product']['slug']
		            		),
		            		'alt' => $item['Product']['name'],
		            		'width' => 60,
		            		'height' => 60,
		            		'class' => 'image'
		            	)
		            ); ?>
				</td>
				<td class="col col-sm-7">
					<strong><?php echo $this->Html->link($item['Product']['name'], array('controller' => 'products', 'action' => 'view', 'slug' => $item['Product']['slug'])); ?></strong>
				</td>
				<td class="col col-sm-1 center" id="price-<?php echo $key; ?>">
					<?php echo $item['Product']['price']; ?><?php echo Configure::read('Settings.CURRENCY'); ?>
				</td>
				<td class="col col-sm-1">
					<?php echo $this->Form->input('quantity-' . $key,
							array(
								'div' => false,
								'class' => 'numeric form-control input-small',
								'label' => false,
								'size' => 2,
								'maxlength' => 2,
								'tabindex' => $tabindex++,
								'data-id' => $item['Product']['id'],
								'value' => $item['quantity']
							)
					); ?>
				</td>
				<td class="col col-sm-1 center" id="subtotal_<?php echo $key; ?>">
					<?php echo $item['subtotal']; ?><?php echo Configure::read('Settings.CURRENCY'); ?>
				</td>
				<td class="col col-sm-1 center">
					<span class="remove" id="<?php echo $key; ?>"></span>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<div class="row">
	</div>

	

		<div class="row" id="row-<?php echo $key; ?>">

		</div>
</div><!-- /.shopping-cart-items -->

<hr>

<div class="row">
	<div class="col col-sm-12">
		<div class="pull-right">
		<?php echo $this->Html->link('<i class="icon-remove icon"></i>&nbsp;'.__('Clear Cart'),
				array('controller' => 'shop', 'action' => 'clear'),
				array('class' => 'btn btn-danger', 'escape' => false)
		); ?>
		<?php echo $this->Form->button('<i class="icon-refresh icon"></i>&nbsp;'.__('Recalculate'), array('class' => 'btn btn-default', 'escape' => false));?>
		<?php echo $this->Form->end(); ?>
		</div>
	</div>
</div>

<hr>

<div class="row">
	<div class="col col-sm-12 pull-right tr">
		
		<p class="cart-total">
			<?php echo __('Order Total') ?>: <span id="total"><?php echo $shop['Order']['total']; ?><?php echo Configure::read('Settings.CURRENCY'); ?></span>
		</p>

		<?php echo $this->Html->link(
			'<i class="glyphicon glyphicon-arrow-right"></i>&nbsp;'.__('Proceed to Checkout'),
			array('controller' => 'shop', 'action' => 'endOfDemo'),
			array('class' => 'btn btn-primary', 'escape' => false)
		); ?>
	</div>
</div>

<br />
<br />

<?php endif; ?>
