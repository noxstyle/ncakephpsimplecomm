$(document).ready(function(){
    $.messages = {
        cart: {
            productAdded: 'Tuote lisätty ostoskoriin', // 'Product added to cart'
            productAddingFailed: '', // 'Unable to add product to cart'
            productRemoved: 'Tuote poistettu ostoskorista', // 'Product removed from cart'
            productRemovalFailed: 'Tuotteen poistaminen epäonnistui' // 'Unable to remove the product'
        }
    };
});
