/**
 *
 */
!function(e){
    "use strict";

    $(document).ready(function(){

        // Make cart follow window scroll
        var $sidebar   = $("#sidebar"), 
            $window    = $(window),
            offset     = $sidebar.offset(),
            maxScroll  = $('.footer').offset(),
            topPadding = 15;

        $window.scroll(function() {
            if ($window.scrollTop() > offset.top) {
                $sidebar.stop().animate({
                    marginTop: $window.scrollTop() - offset.top + topPadding
                });
            } else {
                $sidebar.stop().animate({
                    marginTop: 0
                });
            }
        });

        // Initialize "remove from cart event"
        hookRemoveEvent();

        // Attach addToCart event
        $('.add-cart-item').on('click',function(e){
            e.preventDefault();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: $(this).attr('href'),
                data: { id: $(this).attr('data-id') },
                success:function(d){
                    if (d.status) {
                        $('#cart-holder').html(d.html);
                        showTimedStatus(2, $.messages.cart.productAdded, '#cart-status');
                        hookRemoveEvent();
                    };
                },
                error: function() {
                    // Handle errors
                }
            });
        });
    });

    var hookRemoveEvent = function() {
        $('#cart-holder a.remove-cart-item').on('click',function(e){
            e.preventDefault();
            var parentEl = $(this).parent().parent();
            $.ajax({
                url: $(this).attr('href'),
                type:'POST',
                dataType:'json',
                success:function(d){
                    if (d.status) {
                        parentEl.remove();
                        $('#cart-total-sum').html(d.total);
                        showTimedStatus(2, $.messages.cart.productRemoved, '#cart-status');
                    }
                },
                error: function() {
                    // Handle errors
                }
            });
        });
    };

    var showTimedStatus = function(status, msg, element, duration) {
        duration = duration || 3000;
        var alert = $('<div class="alert"></div>');

        switch(status)
        {
            case 1: alert.addClass('alert-success'); break;
            case 2: alert.addClass('alert-warning'); break;
            case 3: alert.addClass('alert-info'); break;
            default: alert.addClass('alert-danger'); break;
        }

        alert.html(msg);
        $(element).html(alert);

        clearTimeout($.cartNotification);
        $.cartNotification = setTimeout(function(){
            $('.alert', element)
              .slideUp(500).remove();
        }, duration);
    };

}(window.jQuery);