<?php
App::uses('AppController', 'Controller');
class ShopController extends AppController {

	public $components = array(
		'Cart',
		'Paypal',
		'AuthorizeNet'
	);

	public $uses = 'Product';

	public function beforeFilter() {
		parent::beforeFilter();
		$this->disableCache();
		$this->layout = 'no_side_bar';
		//$this->Security->validatePost = false;
	}

	public function clear() {
		$this->Cart->clear();
		$this->Session->setFlash(__('All item(s) removed from your shopping cart'), 'flash_error');
		return $this->redirect('/');
	}

	/**
	 * 
	 */
	public function add() {
		if ($this->request->is('post')) {

			$productId = (int)$this->request->data['id'];
			$quantity = isset($this->request->data['Product']['quantity']) ? $this->request->data['Product']['quantity'] : 1;

			# Check if there is existing instances of  the product
			$cart = $this->Session->read('Shop');
			if (isset($cart['OrderItem']) && count($cart['OrderItem']) > 0)
			{
				foreach ($cart['OrderItem'] as $key => $item) {
					if ($item['product_id'] == $productId)
						$quantity = $item['quantity'] + 1;
				}
			}
			
			$product = $this->Cart->add($productId, $quantity, null);
		}

		if ($this->request->is('ajax'))
			$this->finalizeAddAjax($product);
		else
			$this->finalizeAddStandard($product);
	}

	/**
	 * @param Product $product
	 */
	protected function finalizeAddAjax($product) {
		$view = new View($this, false);
		$cart = $this->Session->read('Shop');
		$html = $view->element('cart', array('cart'=>$cart));

		echo json_encode(array(
			'status' => (!empty($product)) ? 1 : 0,
			'html' => $html,
		));
		$this->autoRender = false;
	}

	/**
	 * @param Product $product
	 */
	protected function finalizeAddStandard($product) {
		if(!empty($product)) {
			$this->Session->setFlash($product['Product']['name'] . ' was added to your shopping cart.', 'flash_success');
		} else {
			$this->Session->setFlash('Unable to add this product to your shopping cart.', 'flash_error');
		}
		$this->redirect($this->referer());
	}

	public function itemupdate() {
		if ($this->request->is('ajax')) {

			$id = $this->request->data['id'];

			$quantity = isset($this->request->data['quantity']) ? $this->request->data['quantity'] : null;

			if(isset($this->request->data['mods']) && ($this->request->data['mods'] > 0)) {
				$productmodId = $this->request->data['mods'];
			} else {
				$productmodId = null;
			}

			$product = $this->Cart->add($id, $quantity, $productmodId);

		}
		$cart = $this->Session->read('Shop');
		echo json_encode($cart);
		$this->autoRender = false;
	}

	public function update() {
		$this->Cart->update($this->request->data['Product']['id'], 1);
	}

	/**
	 * 
	 */
	public function remove($id = null) {
		$product = $this->Cart->remove($id);
		if ($this->request->is('ajax')) {

			# Re-read the cart from session in order to pass the updateable total sum to view
			$cart = $this->Session->read('Shop');

			echo json_encode(array(
				'status' => (!empty($product)) ? 1 : 0,
				'total' => $cart['Order']['total']
			));

			$this->autoRender = false;
		} else {
			if(!empty($product))
				$this->Session->setFlash($product['Product']['name'] . ' was removed from your shopping cart', 'flash_error');

			return $this->redirect(array('action' => 'cart'));
		}
	}

	public function cartupdate() {
		if ($this->request->is('post')) {
			foreach($this->request->data['Product'] as $key => $value) {
				$p = explode('-', $key);
				$p = explode('_', $p[1]);
				$this->Cart->add($p[0], $value, $p[1]);
			}
			$this->Session->setFlash('Shopping Cart is updated.', 'flash_success');
		}
		return $this->redirect(array('action' => 'cart'));
	}

	public function cart() {
		$shop = $this->Session->read('Shop');
		$this->set(compact('shop'));
	}

	/**
	 * End of demo functionality text
	 */
	public function endOfDemo() {
		# Redirect empty cart to mainpage
		$shop = $this->Session->read('Shop');
		if(!$shop['Order']['total'])
			return $this->redirect('/');
	}
}