<?php
App::uses('AppController', 'Controller');
class UsersController extends AppController {

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('login');
	}

	public function login() {

		// echo AuthComponent::password('admin');

		if ($this->request->is('post')) {
			if($this->Auth->login()) {

				$this->User->id = $this->Auth->user('id');
				$this->User->saveField('logins', $this->Auth->user('logins') + 1);
				$this->User->saveField('last_login', date('Y-m-d H:i:s'));

				if ($this->Auth->user('role') == 'customer') {
					return $this->redirect(array(
						'controller' => 'users',
						'action' => 'dashboard',
						'customer' => true,
						'admin' => false
					));
				} elseif ($this->Auth->user('role') == 'admin') {
					return $this->redirect(array(
						'controller' => 'users',
						'action' => 'dashboard',
						'manager' => false,
						'admin' => true
					));
				} else {
					$this->Session->setFlash(__('Login is incorrect'));
				}
			} else {
				$this->Session->setFlash(__('Login is incorrect'));
			}
		}
	}

	public function logout() {
		$this->Session->setFlash(__('Good-Bye'));
		return $this->redirect($this->Auth->logout());
	}

	public function admin_dashboard() {
	}

	public function admin_index() {

		$this->Paginator = $this->Components->load('Paginator');

		$this->Paginator->settings = array(
			'User' => array(
				'recursive' => -1,
				'contain' => array(
				),
				'conditions' => array(
				),
				'order' => array(
					'Users.name' => 'ASC'
				),
				'limit' => 20,
				'paramType' => 'querystring',
			)
		);
		$users = $this->Paginator->paginate();
		$this->set(compact('users'));
	}
}
