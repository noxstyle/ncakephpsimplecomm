��    X      �     �      �     �     �     �     �     �     �  +   �  %     	   *     4     E  
   J  
   U     `     i  
   z     �     �     �  
   �     �     �     �     �     �     �     �     �     �     �     	     	     	     	     .	     >	     Q	  
   b	  
   m	     x	  H   �	  :   �	     

  y   
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
                         &     4     K     O     b     r     w     �     �     �     �     �  2   �       ,   5     b     |  ,   �  *   �     �     �  	   �                    $     5     :     ?  w  H  
   �     �     �     �     �       (        >     ^     f  	   z     �  
   �  	   �     �     �     �     �     �     �     �     �       	              %     *     0     6     H     _     m     v     {     �     �     �     �     �     �  E   �  7   D     |  �   �     -     ;     D     J     \     i     y     �     �     �     �     �     �     �  	   �     �     �     �          0     7     P  
   c  	   n     x     �  .   �     �  ,   �          6  .   P  -        �     �     �     �     �     �       	        !  	   *     T          U          E             
          J   &   D   $   S              +   6       3               /          Q             0       :           >      W   K   2   7      8   .   V              *      4      =      R       F   O                 ;   5       9               C   M          '              ?       ,   (             "          X                  I   -   )              %           #       G   @       L   A   	          N   !   <   P         H   1   B       Active Add Category Add Product Add to cart Added All Statuses All item(s) removed from your shopping cart Are you sure you want to delete # %s? Ascending Back to mainpage Cart Cart total Categories Category Category deleted Clear Cart Create Created Current Image Descending Description Edit Edit Product: %s Good-Bye Home Image Item Lft Login Login is incorrect Logout Modified Name Name already exists Name is invalid Name is not uniqie Name is required No Results Not Active Not logged in Only alphabetic letters, dashes and underscores, between 3-50 characters Only lowercase letters and dashes, between 3-50 characters Order Total Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end} Parent Category Password Price Price is invalid Proceed to Checkout Product deleted Products Qty Quantity Recalculate Remove Reset Search Rght Search Shopping Cart Shopping Cart is empty Sku Sku already exists Sku is required Slug Slug already exists Slug is required Sort by Subtotal The category has been created The category has been saved The product could not be saved. Please, try again. The product has been saved Unable to create category. Please try again. Unable to delete category Unable to delete product Unable to modify category. Please try again. Unable to save product. Please, try again. Username View View cart Views Your cart is empty first found %s results last next previous Project-Id-Version: PROJECT VERSION
POT-Creation-Date: 2013-12-05 17:25+0200
PO-Revision-Date: 2013-12-05 18:11+0200
Last-Translator: Joni <joni.lepisto@noxstyle.info>
Language-Team: LANGUAGE <EMAIL@ADDRESS>
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;
X-Generator: Poedit 1.5.4
 Aktiivinen Lisää kategoria Lisää tuote Lisää ostoskoriin Lisätty Kaikki tilat Kaikki tuoteet on poistettu ostoskorista Haluatko varmasti poistaa # %s? Nouseva Takaisin etusivulle Ostoskori Ostoskorin hinta Kategoriat Kategoria Kategoria poistettu Tyhjennä ostoskori Luo Luotu Nykyinen kuva Laskeva Kuvaus Muokkaa Muokkaa tuotetta: %s Näkemiin Koti Kuva Tuote Vasen Kirjaudu sisään Kirjautuminen väärin Kirjaudu ulos Muokattu Nimi Nimi on jo käytössä Nimi on väärin Nimi on jo käytössä Nimi on vaadittu kenttä Ei tuloksia Ei aktiivinen Ei kirjautuneena Vain alfanumeeriset merkit, viivat ja alaviivat, pituus 3-50 merkkiä Vain pieniä kirjaimia ja viivoja, pituus 3-50 merkkiä Tilauksen hinta yhteensä Sivulla {:page} kaikkiaan {:pages} sivusta, näytetään {:current} tietuetta {:count}sta, alkaen tietueesta {:start} ja päättyen tietueeseen {:end} Yläkategoria Salasana Hinta Hinta on väärin Maksa tilaus Tuote poistettu Tuotteet Määrä Määrä Laske hinta uudelleen Poista Nollaa haku Oikea Etsi Ostoskori Ostoskori on tyhjä Tuotenumero Tuotenumero on jo käytössä Tuotenumero on vaadittu kenttä Tynkä Tynkä on jo käytössä Tynkä on vaadittu Järjestä Yhteensä Uusi kategoria luotu Kategoria muokkaus onnistui Tuotetta ei voitu tallentaa, Yritä uudelleen. Tuote on tallennettu Kategoriaa ei voitu luoda. Yritä uudelleen. Kategoriaa ei voitu poistaa Tuotetta ei voitu poistaa Kategoriaa ei voitu muokata. Yritä uudelleen. Tuotetta ei voitu tallentaa. Yritä uudelleen Käyttäjänimi Näytä Näytä ostoskori Näyttöjä Ostoskori on tyhjä ensimmäinen löytyi %s tulosta viimeinen seuraava edellinen 